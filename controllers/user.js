const { response } = 'express';

const usuarioGet = (req, res = response) => {
  //const query = req.query
  const { q, nombre, key = 'no key'  } = req.query;

  res.json({
    msg: 'get API - controlador',
    q,
    nombre,
    key
  });
};

const usuarioPut = (req, res = response) => {
  //const id = req.params.id;
  const { id } = req.params;

  res.json({
    msg: 'put API - controlador',
    id
  });
};

const usuarioPost = (req, res) => {
  //req.body trae la informacion del body
  const {nombre, edad} = req.body;
  
  res.status(201).json({
    msg: 'post API',
    nombre,
    edad
  });
}


const usuarioDelete = (req, res = response) => {
  res.json({
    msg: 'delete API - controlador'
  });
};

module.exports = {
  usuarioGet,
  usuarioPut,
  usuarioPost,
  usuarioDelete
};
