const { Router } = require('express');

const router = Router();

const { usuarioGet,
  usuarioPut,
  usuarioPost,
  usuarioDelete } = require('../controllers/user')

router.get('/', usuarioGet)

router.put('/:id', usuarioPut);

router.post('/', usuarioPost);

router.delete('/', usuarioDelete);

module.exports = router;
